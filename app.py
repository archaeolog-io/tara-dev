from werkzeug.serving import run_simple
from werkzeug.wsgi import DispatcherMiddleware

from tara.api import controller as api_controller
from tara import frontend as frontend_controller

# create application dispatcher for endpoints
app = DispatcherMiddleware(frontend_controller.create_app(), {
    '/api': api_controller.create_app()
})

# run development server
if __name__ == '__main__':
    run_simple('0.0.0.0', 5000, app, use_reloader=True, use_debugger=True)
