import unittest
import json

from tara.api import controller as api

UNITTEST_TEMPLATE = dict(
    label='_unittestname_',
    parent=None,
    grid_fields=[
        {
            "name": "textfield",
            "label": "Textfield",
            "type": "string"
        },
        {
            "name": "textareafield",
            "label": "Textareafield",
            "type": "string"
        }
    ],
    form_fields=[
        {
            "name": "textfield",
            "label": "Textfield",
            "type": "text"
        },
        {
            "name": "textareafield",
            "label": "Textareafield",
            "type": "textarea"
        }
    ]
)

UNITTEST_RESOURCE = dict(
    textfield='text',
    textareafield='textarea'
)


class ResourceTestCase(unittest.TestCase):
    def setUp(self):
        app = api.create_app(TESTING=True)
        self.client = app.test_client()

    def test_create_resource(self, cleanup=True):
        # create template
        json_string = json.dumps(UNITTEST_TEMPLATE)
        rv = self.client.post('/templates/', data=json_string, content_type='application/json')
        self.assertEqual(rv.status_code, 200, rv.data)
        self.assertEqual(rv.content_type, 'application/json')

        # get template id
        template_id = json.loads(rv.data)['_id']
        self.assertEqual(len(template_id), 24)

        # create resource
        url = '/resources/{0}/'.format(template_id)
        json_string = json.dumps(UNITTEST_RESOURCE)
        rv = self.client.post(url, data=json_string, content_type='application/json')

        # verify result
        self.assertEqual(rv.status_code, 200, rv.data)
        self.assertEqual(rv.content_type, 'application/json')

        # get resource id
        _id = json.loads(rv.data)['_id']
        self.assertEqual(len(_id), 24)

        # update resource
        url = '/resources/{0}/{1}'.format(template_id, _id)
        json_string = json.dumps(UNITTEST_RESOURCE)
        rv = self.client.patch(url, data=json_string, content_type='application/json')
        self.assertEqual(rv.status_code, 200, rv.data)
        self.assertEqual(rv.content_type, 'application/json')

        # delete resource
        rv = self.client.delete(url)
        self.assertEqual(rv.status_code, 200, rv.data)

        # delete non-existing resource
        # rv = self.client.delete(url)
        # self.assertEqual(rv.status_code, 404, rv.data)

        # try retrive deleted resource
        # rv = self.client.get(url)
        # self.assertEqual(rv.status_code, 404)

        # delete template
        rv = self.client.delete('/templates/' + template_id)
        self.assertEqual(rv.status_code, 200)

    def test_autocomplete(self):
        # create template
        json_string = json.dumps(UNITTEST_TEMPLATE)
        rv = self.client.post('/templates/', data=json_string, content_type='application/json')
        self.assertEqual(rv.status_code, 200, rv.data)
        self.assertEqual(rv.content_type, 'application/json')

        # get template id
        template_id = json.loads(rv.data)['_id']
        self.assertEqual(len(template_id), 24)

        # create resource
        url = '/resources/{0}/'.format(template_id)
        json_string = json.dumps(UNITTEST_RESOURCE)
        rv = self.client.post(url, data=json_string, content_type='application/json')

        # verify result
        self.assertEqual(rv.status_code, 200, rv.data)
        self.assertEqual(rv.content_type, 'application/json')

        # get resource id
        _id = json.loads(rv.data)['_id']
        self.assertEqual(len(_id), 24)

        # testautocomplete
        url = '/autocomplete/{0}/{1}'.format(template_id, UNITTEST_TEMPLATE['form_fields'][0]['name'])
        rv = self.client.get(url, content_type='application/json')

        # verify result
        self.assertEqual(rv.status_code, 200, rv.data)
        self.assertEqual(rv.content_type, 'application/json')

        data = json.loads(rv.data)['result']
        self.assertEqual(len(data), 1)
        self.assertEqual(data[0], UNITTEST_RESOURCE['textfield'])


        # delete resource
        url = '/resources/{0}/{1}'.format(template_id, _id)
        rv = self.client.delete(url)
        self.assertEqual(rv.status_code, 200, rv.data)

        # delete template
        rv = self.client.delete('/templates/' + template_id)
        self.assertEqual(rv.status_code, 200)


if __name__ == '__main__':
    unittest.main()