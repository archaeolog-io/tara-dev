import unittest
import json

from tara.api import controller as api

UNITTEST_CATEGORY_0 = dict(
    name='category 0',
    sort=0
)

UNITTEST_CATEGORY_1 = dict(
    name='category 1',
    sort=1
)


class CategoryTestCase(unittest.TestCase):
    def setUp(self):
        app = api.create_app(TESTING=True)
        self.client = app.test_client()

    def test_create_category(self):
        # create category
        json_string = json.dumps(UNITTEST_CATEGORY_0)
        rv = self.client.post('/categories/', data=json_string, content_type='application/json')
        self.assertEqual(rv.status_code, 200, rv.data)
        self.assertEqual(rv.content_type, 'application/json')

        # get category id
        category_id = json.loads(rv.data)['_id']
        self.assertEqual(len(category_id), 24)

        # update category
        url = '/categories/{0}'.format(category_id)
        json_string = json.dumps(UNITTEST_CATEGORY_1)
        rv = self.client.patch(url, data=json_string, content_type='application/json')
        self.assertEqual(rv.status_code, 200, rv.data)
        self.assertEqual(rv.content_type, 'application/json')

        # delete category
        rv = self.client.delete(url)
        self.assertEqual(rv.status_code, 200, rv.data)


if __name__ == '__main__':
    unittest.main()
