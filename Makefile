# clean db
clean:
	@mongo tara --eval "db.dropDatabase()"

# sync db
sync:
	@ssh sander@tara.ut.ee mongoexport -d tara -c users | mongoimport -d tara -c users
	@ssh sander@tara.ut.ee mongoexport -d tara -c roles | mongoimport -d tara -c roles
	@ssh sander@tara.ut.ee mongoexport -d tara -c templates | mongoimport -d tara -c templates
	@ssh sander@tara.ut.ee mongoexport -d tara -c template_layouts | mongoimport -d tara -c template_layouts
	@ssh sander@tara.ut.ee mongoexport -d tara -c classificators | mongoimport -d tara -c classificators
	@ssh sander@tara.ut.ee mongoexport -d tara -c categories | mongoimport -d tara -c categories

# stat
stat:
	@echo -n -e "python:\t\t"
	@find . -name '*.py' -not -path './venv/*' | xargs wc -l | tail -1 | sed 's/^ *//g'

	@echo -n -e 'javascript:\t'
	@find . -name '*.js' -not -path './venv/*' -not -path './tara/frontend/static/js/libs/*' | xargs wc -l | tail -1 | sed 's/^ *//g'

	@echo -n -e 'html:\t\t'
	@find . -name '*.html' -not -path './venv/*' | xargs wc -l | tail -1 | sed 's/^ *//g'
