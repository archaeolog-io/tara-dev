define(['text!/static/templates/helpers/metadata.html', 'backbone-forms'], function (fileReferenceEditorTmpl) {
  "use strict";

  var MetadataEditor =  Backbone.Form.editors.Base.extend({
    events: {
      'click #add-reference': function (e) {
        e.preventDefault();

        // find last class
        var last = this.$el.find('.form-inline:last');
        var nr = 0;

        if(last.length) {
          nr = parseInt(last.attr('data-socket')) + 1;
          last.after('<div class="clearfix"></div><br>' + this.$el.find('#referenceRow').html());
        } else {
          this.$el.prepend(this.$el.find('#referenceRow').html() + '<div class="clearfix"></div><br>');
        }

        // find new last and update socket
        last = this.$el.find('.form-inline:last');
        last.attr('data-socket', nr);
      },

      // remove item
      'click .form-inline > button': function (e) {
        var self = this;
        e.preventDefault();
        $(e.target).parent().remove();
      }
    },

    getValue: function () {
      var result = {};
      this.$el.find('.form-inline').each(function (index, value) {
        var elem = $(value);
        var key = elem.find('input').val();
        if(key) {
          result[key] = elem.find('textarea').val();
        }
      });
      return result;
    },

    render: function () {
      this.$el.html(_.template(fileReferenceEditorTmpl, {templates: this.templates}));
      var reference_list = this.$el.find('#reference-list');
      var self = this;

      var index = 0;
      _.each(this.value, function (value, key) {
        reference_list.append(self.$el.find('#referenceRow').html() + '<div class="clearfix"></div><br>');
        var elem = reference_list.find('.form-inline:last');
        elem.attr('data-socket', index);
        elem.find('input').val(key);
        elem.find('textarea').val(value);
        index++;
      });

      return this;
    }
  });

  return MetadataEditor;
});
