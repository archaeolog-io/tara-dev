define(['backbone', 'models/autocomplete', 'backbone-forms'], function (Backbone, AutocompleteModel) {
  "use strict";

  var InputAutocompleteEditor = Backbone.Form.editors.Text.extend({
    initialize: function (options) {
      Backbone.Form.editors.Text.prototype.initialize.call(this, options);

      var autocompleteModel = new AutocompleteModel({}, {templateId: this.schema.autocomplete.template_id, field: this.schema.autocomplete.field});

      this.$el.typeahead({source: function (query, process) {
        autocompleteModel.fetch({success: function () {
          process(autocompleteModel.toJSON().result);
        }});
      }});
    }
  });

  return InputAutocompleteEditor;
});
