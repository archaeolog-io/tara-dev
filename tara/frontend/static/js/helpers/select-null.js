define(['backbone', 'backbone-forms'], function (Backbone) {
  "use strict";

  var SelectNullEditor = Backbone.Form.editors.Select.extend({
    getValue: function () {
      return this.$el.val() || null;
    }
  });

  return SelectNullEditor;
});
