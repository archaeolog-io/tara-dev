define(['backbone', 'backbone-forms'], function (Backbone) {
  "use strict";

  var TextAreaJSONEditor = Backbone.Form.editors.TextArea.extend({

    initialize: function(options) {
      // Call parent constructor
      if (!options.schema) {
        options.schema = {};
      }

      if (!options.schema.editorAttrs) {
        options.schema.editorAttrs = {};
      }

      if (!options.schema.editorAttrs.style) {
        options.schema.editorAttrs.style = 'font-family: monospace; width: 600px; height: 400px';
      } else {
        options.schema.editorAttrs.style += ' font-family: monospace;';
      }

      Backbone.Form.editors.Base.prototype.initialize.call(this, options);
    },

    getValue: function () {
      var value = this.$el.val();
      if (value) {
        return JSON.parse(value);
      }
    },

    setValue: function (value) {
      if (value) {
        var str = JSON.stringify(value, null, '  ');
        this.$el.val(str);
      }
    }
  });

  return TextAreaJSONEditor;
});
