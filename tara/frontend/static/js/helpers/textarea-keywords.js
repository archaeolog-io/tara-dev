define(['backbone', 'backbone-forms'], function (Backbone) {
  "use strict";

  var TextAreaKeywordsEditor = Backbone.Form.editors.TextArea.extend({

    initialize: function(options) {
      // Call parent constructor
      Backbone.Form.editors.Base.prototype.initialize.call(this, options);
    },

    getValue: function () {
      var value = this.$el.val();
      if (value) {
        return value.split(/[, ]+/);
      } else {
        return [];
      }
    },

    setValue: function (value) {
      if (value) {
        var str = value.join(', ');
        this.$el.val(str);
      }
    }
  });

  return TextAreaKeywordsEditor;
});
