define(['backbone', 'backbone-forms'], function (Backbone) {
  "use strict";

  var InputFileEditor = Backbone.Form.editors.Text.extend({
    initialize: function (options) {
      Backbone.Form.editors.Base.prototype.initialize.call(this, options);
      this.$el.attr('type', 'file');
    },

    getValue: function () {
      return this.$el[0].files[0] || null;
    }
  });

  return InputFileEditor;
});
