define({
  'root': {
    'base-login': 'Log-in',
    'base-change-password': 'Change password',
    'base-templates': 'Templates',
    'base-layouts': 'Layouts',
    'base-users': 'Users',
    'base-roles': 'Roles',
    'base-categories': 'Cateogires',
    'base-classificators': 'Classificators',
    'base-logout': 'Logout',

    'meta-added': 'Added',
    'meta-updated': 'Updated',

    'button-save': 'Save',
    'button-delete': 'Delete',

    'title-add-category': 'Add category',
    'title-edit-category': 'Edit category'
  }
});
