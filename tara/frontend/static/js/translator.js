define(['i18n!nls/translation'], function (translation) {
  var T = function (name) {
    return name in translation ? translation[name] : 'UNDEFINED: ' + name;
  };
  return T;
});
