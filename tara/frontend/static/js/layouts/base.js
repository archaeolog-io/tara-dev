define(['backbone', 'collections/category', 'text!/static/templates/base.html', 'text!/static/templates/template/menu.html'], function (Backbone, CategoryCollection, tmpl, menu) {
  "use strict";

  var BaseLayout = Backbone.View.extend({
    el: 'body',

    initialize: function (options) {
      var self = this;

      this.templateCollection = options.templateCollection;

      var html = _.template(tmpl);
      this.$el.append(html);

      var categoriesCollection = new CategoryCollection();
      categoriesCollection.fetch({success: function () {
        var tmpl = _.template(menu);
        var html = tmpl({categories: categoriesCollection});
        self.$el.find('#categories').html(html);
      }});

      this.$el.find('#sidebar-collapse').bind('click', function (e) {
        e.preventDefault();
        $('#sidebar').toggleClass('menu-min');
      });
    },

    render: function (view) {
      if (this._currentView) {
        this._currentView.unbind();
        this._currentView.undelegateEvents();
        if (this._currentView.onClose) {
          this._currentView.onClose();
        }
        this._currentView.remove();
      }
      this._currentView = view;
      var self = this;
      this.templateCollection.fetch({reset: true, success: function () {
        // rewrite content
        self.$el.find('#content-rewrite').html(view.render().el);
        view.delegateEvents();

        if (Backbone.User.is_authenticated()) {
          if (Backbone.User.get_info().username == 'anonymous') {
            self.$el.find('#admin_change_password').hide();
          } else {
            self.$el.find('#admin_change_password').show();
          }

          self.$el.find('.authenticated-only').show();
          self.$el.find('.anonymous-only').hide();
          self.$el.find('#username-field').html(Backbone.User.get_info().email);

          if (Backbone.User.permissions('/templates').PATCH) {
            self.$el.find('#admin_templates').show();
          }

          if (Backbone.User.permissions('/templates/layouts').PATCH) {
            self.$el.find('#admin_templates_layouts').show();
          }

          if (Backbone.User.permissions('/users').PATCH) {
            self.$el.find('#admin_users').show();
          }

          if (Backbone.User.permissions('/roles').PATCH) {
            self.$el.find('#admin_roles').show();
          }

          if (Backbone.User.permissions('/categories').PATCH) {
            self.$el.find('#admin_categories').show();
          }

          if (Backbone.User.permissions('/classificators').PATCH) {
            self.$el.find('#admin_classificators').show();
          }
        } else {
          self.$el.find('#admin_templates').hide();
          self.$el.find('#admin_templates_layouts').hide();
          self.$el.find('#admin_users').hide();
          self.$el.find('#admin_roles').hide();
          self.$el.find('#admin_categories').hide();
          self.$el.find('#admin_classificators').hide();
          self.$el.find('.authenticated-only').hide();
          self.$el.find('.anonymous-only').show();
        }
      }});
    }
  });

  return BaseLayout;
});
