define(['backbone', 'models/resource', 'backbone-pageable'], function (Backbone, ResourceModel) {
  "use strict";

  //var ResourceCollection = Backbone.Collection.extend({
  var ResourceCollection = Backbone.PageableCollection.extend({
// https://github.com/wyuenho/backbone-pageable/issues/101#issuecomment-24965476
//    set: function(models, options) {
//      options = options || {};
//      delete options.url;
//      Backbone.PageableCollection.prototype.set.call(this, models, options);
//    },

    model: ResourceModel,

    queryParams: {
      directions: {
        '-1': 1,
        '1': -1
      }
    },

    initialize: function (models, options) {
      if(options.templateId) {
        this.templateId = options.templateId;
      }
    },

    url: function () {
      return '/api/resources/' + this.templateId + '/';
    },

    parseRecords: function (response) {
      return response.result;
    },

    parseState: function (response) {
      return {totalRecords: response.total_entries};
    }
  });

  return ResourceCollection;
});
