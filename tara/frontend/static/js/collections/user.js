define(['backbone', 'models/user'], function (Backbone, UserModel) {
  "use strict";

  var UserCollection = Backbone.Collection.extend({
    url: '/api/users/',

    model: UserModel,

    parse: function (response) {
      return response.result;
    }
  });

  return UserCollection;
});
