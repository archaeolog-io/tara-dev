define(['backbone', 'models/template_layout'], function (Backbone, TemplateLayoutModel) {
  "use strict";

  var TemplateLayoutCollection = Backbone.Collection.extend({
    url: '/api/templates/layouts/',

    model: TemplateLayoutModel,

    parse: function (response) {
      return response.result;
    }
  });

  return TemplateLayoutCollection;
});
