define(['backbone', 'views/classificator/form', 'models/classificator', 'text!/static/templates/classificator/edit.html', 'translator'], function (Backbone, ClassificatorForm, ClassificatorModel, classificatorEditTmpl, T) {
  "use strict";

  var ClassificatorEditView = Backbone.View.extend({
    template: _.template(classificatorEditTmpl),

    events: {
      'click #save-item': function (e) {
        e.preventDefault();
        var errors = this.form.commit();
        if (!errors) {
          Backbone.history.navigate('classificators', {trigger: true});
        }
      },
      'click #delete-item': function (e) {
        e.preventDefault();
        if(confirm(T('confirm-delete'))) {
          this.model.destroy({success: function () {
            Backbone.history.navigate('classificators', {trigger: true});
          }});
        }
      }
    },

    initialize: function (options) {
      this.model = new ClassificatorModel({_id: options.classificatorId});
      this.listenTo(this.model, 'sync', this.redirect);
    },

    render: function () {
      var self = this;
      this.model.fetch({save: false, success: function () {
        self.form = new ClassificatorForm({template: self.template, model: self.model});
        self.$el.append(self.form.render().el);
      }});
      return this;
    },

    onClose: function () {
      if (this.form) {
        this.form.remove();
      }
    }
  });

  return ClassificatorEditView;
});
