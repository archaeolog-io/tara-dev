define(['backbone', 'views/category/form', 'models/category', 'text!/static/templates/category/edit.html', 'translator'], function (Backbone, CategoryForm, CategoryModel, categoryEditTmpl, T) {
  "use strict";

  var CategoryEditView = Backbone.View.extend({
    template: _.template(categoryEditTmpl),

    events: {
      'click #save-item': function (e) {
        e.preventDefault();
        var errors = this.form.commit();
        if (!errors) {
          Backbone.history.navigate('categories', {trigger: true});
        }
      },
      'click #delete-item': function (e) {
        e.preventDefault();
        if(confirm(T('confirm-delete'))) {
          this.model.destroy({success: function () {
            Backbone.history.navigate('categories', {trigger: true});
          }});
        }
      }
    },

    initialize: function (options) {
      this.model = new CategoryModel({_id: options.categoryId});
      this.listenTo(this.model, 'sync', this.redirect);
    },

    render: function () {
      var self = this;
      this.model.fetch({save: false, success: function () {
        self.form = new CategoryForm({template: self.template, model: self.model});
        self.$el.append(self.form.render().el);
      }});
      return this;
    },

    onClose: function () {
      if (this.form) {
        this.form.remove();
      }
    }
  });

  return CategoryEditView;
});
