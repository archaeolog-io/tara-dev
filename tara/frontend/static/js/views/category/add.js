define(['backbone', 'views/category/form', 'models/category', 'text!/static/templates/category/add.html'],
function (Backbone, CategoryForm, CategoryModel, categoryAddTmpl) {

  "use strict";

  var CategoryAddView = Backbone.View.extend({
    template: _.template(categoryAddTmpl),

    events: {
      'click #save-item': function (e) {
        e.preventDefault();
        var errors = this.form.commit();
        if (!errors) {
          Backbone.history.navigate('categories', {trigger: true});
        }
      }
    },

    initialize: function () {
      var model = new CategoryModel();
      this.form = new CategoryForm({template: this.template, model: model});
    },

    render: function () {
      this.$el.append(this.form.render().el);
      return this;
    },

    onClose: function () {
      this.form.remove();
    }
  });

  return CategoryAddView;
});
