define(['collections/template', 'views/template/grid', 'text!/static/templates/template/list.html'], function (TemplateCollection, TemplateGrid, templateListTmpl) {
  "use strict";

  var TemplateListView = Backbone.View.extend({
    template: _.template(templateListTmpl),
    initialize: function (options) {
      this.grid = new TemplateGrid({collection: options.collection});
    },

    render: function () {
      this.$el.append(this.template({permissions: Backbone.View.permissions()}));
      this.$el.find('#grid-content').append(this.grid.render().$el);
      return this;
    },

    onClose: function () {
      this.grid.remove();
    }
  });

  return TemplateListView;
});
