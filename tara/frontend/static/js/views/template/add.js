define(['backbone', 'collections/template_layout', 'views/template/form', 'models/template', 'text!/static/templates/template/add.html'],
function (Backbone, TemplateLayoutCollection, TemplateForm, TemplateModel, templateAddTmpl) {
  "use strict";

  var TemplateAddView = Backbone.View.extend({
    template: _.template(templateAddTmpl),

    events: {
      'click #save-item': function (e) {
        e.preventDefault();
        this.form.commit();
      }
    },

    initialize: function (options) {
      this.templateCollection = options.templateCollection;

      this.templateLayoutCollection = new TemplateLayoutCollection();
      this.listenTo(this.templateLayoutCollection, 'reset', this.renderForm);

      if(options.templateId) {
        this.cloneTemplateId = options.templateId;
      }
    },

    render: function () {
      this.templateLayoutCollection.fetch({reset: true})
      return this;
    },

    renderForm: function () {
      var model = new TemplateModel();
      this.listenTo(model, 'sync', this.redirect);

      this.form = new TemplateForm({
        template: this.template,
        templateCollection: this.templateCollection,
        templateLayoutCollection: this.templateLayoutCollection,
        model: model
      });

      this.$el.html(this.form.render().el);

      if(this.cloneTemplateId) {
        var m = this.templateCollection.get(this.cloneTemplateId);
        var clone = _.clone(m.attributes);
        delete clone[m.idAttribute];
        delete clone['label'];
        this.form.setValue(clone);
      }
    },

    redirect: function () {
      Backbone.history.navigate('templates', {trigger: true});
    },

    onClose: function () {
      this.form.remove();
    }
  });

  return TemplateAddView;
});
