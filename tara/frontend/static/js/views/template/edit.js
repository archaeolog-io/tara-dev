define(['backbone', 'collections/template_layout', 'views/template/form', 'models/template', 'text!/static/templates/template/edit.html', 'translator'],
function (Backbone, TemplateLayoutCollection, TemplateForm, TemplateModel, templateEditTmpl, T) {
  "use strict";

  var TemplateEditView = Backbone.View.extend({
    template: _.template(templateEditTmpl),

    events: {
      'click #save-item': function (e) {
        e.preventDefault();
        this.form.commit();
      },
      'click #clone-item': function (e) {
        e.preventDefault();
        Backbone.history.navigate('templates/'+this.model.id+'/clone', {trigger: true});
      },
      'click #delete-item': function (e) {
        e.preventDefault();
        if(confirm(T('confirm-delete'))) {
          this.model.destroy({success: function () {
            Backbone.history.navigate('templates', {trigger: true});
          }});
        }
      }
    },

    initialize: function (options) {
      this.templateCollection = options.templateCollection;

      this.templateLayoutCollection = new TemplateLayoutCollection();
      this.listenTo(this.templateLayoutCollection, 'reset', this.layoutReady);

      this.model = new TemplateModel({_id: options.templateId});
      this.listenTo(this.model, 'change', this.renderForm);
      this.listenTo(this.model, 'sync', this.redirect);
    },

    render: function () {
      this.$el.append(this.template({permissions: Backbone.View.permissions()}));
      this.templateLayoutCollection.fetch({reset: true});
      return this;
    },

    layoutReady: function () {
      this.model.fetch({save: false});
    },

    renderForm: function () {
      this.form = new TemplateForm({
        template: _.template('<div data-fields="*"></div>'),
        templateCollection: this.templateCollection,
        templateLayoutCollection: this.templateLayoutCollection,
        model: this.model
      });
      this.$el.find('#form-content').append(this.form.render().el);
    },

    redirect: function (model, resp, options) {
      if (options.patch) {
        Backbone.history.navigate('templates', {trigger: true});
      }
    },

    onClose: function () {
      this.form.remove();
    }
  });

  return TemplateEditView;
});
