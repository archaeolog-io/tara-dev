define(['backbone', 'views/template/layout/form', 'models/template_layout', 'text!/static/templates/template/layout/add.html'],
function (Backbone, TemplateLayoutForm, TemplateLayoutModel, templateLayoutAddTmpl) {

  "use strict";

  var TemplateLayoutAddView = Backbone.View.extend({
    template: _.template(templateLayoutAddTmpl),

    events: {
      'click #save-item': function (e) {
        e.preventDefault();
        this.form.commit();
      }
    },

    initialize: function () {
      var model = new TemplateLayoutModel();
      this.listenTo(model, 'sync', this.redirect);

      this.form = new TemplateLayoutForm({template: this.template, model: model});
    },

    render: function () {
      this.$el.html(this.form.render().el);
      return this;
    },

    redirect: function () {
      Backbone.history.navigate('templates/layouts', {trigger: true});
    },

    onClose: function () {
      this.form.remove();
    }
  });

  return TemplateLayoutAddView;
});
