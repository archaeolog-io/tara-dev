define(['backbone', 'views/template/layout/form', 'models/template_layout', 'text!/static/templates/template/layout/edit.html', 'translator'],
function (Backbone, TemplateLayoutForm, TemplateLayoutModel, templateLayoutEditTmpl, T) {

  "use strict";

  var TemplateLayoutEditView = Backbone.View.extend({
    template: _.template(templateLayoutEditTmpl),

    events: {
      'click #save-item': function (e) {
        e.preventDefault();
        this.form.commit();
      },
      'click #delete-item': function (e) {
        e.preventDefault();
        if(confirm(T('confirm-delete'))) {
          this.model.destroy({success: function () {
            Backbone.history.navigate('templates/layouts', {trigger: true});
          }});
        }
      }
    },

    initialize: function (options) {
      this.model = new TemplateLayoutModel({_id: options.layoutId});
      this.listenTo(this.model, 'sync', this.redirect);
    },

    render: function () {
      var self = this;
      this.$el.append(this.template({permissions: Backbone.View.permissions()}));

      this.model.fetch({save: false, success: function () {
        self.form = new TemplateLayoutForm({template: _.template('<div data-fields="*"></div>'), model: self.model});
        self.$el.find('#form-content').append(self.form.render().el);
      }});
      return this;
    },

    redirect: function (model, resp, options) {
      if (options.patch) {
        Backbone.history.navigate('templates/layouts', {trigger: true});
      }
    },

    onClose: function () {
      this.form.remove();
    }
  });

  return TemplateLayoutEditView;
});
