define(['models/autocomplete', 'collections/resource', 'views/file/reference_row', 'text!/static/templates/file/reference_editor.html', 'text!/static/templates/file/reference_row.html', 'backbone-forms'], function (AutocompleteModel, ResourceCollection, FileReferenceRow, fileReferenceEditorTmpl, fileReferenceRowTmpl) {
  'use strict';

  var FileReferenceEditor =  Backbone.Form.editors.Base.extend({
    counter: 0,

    _references: {},

    tagName: 'ul',

    className: 'reference-manager',

    events: {
      'click #add-reference': function (e) {
        e.preventDefault();
        this.$el.find('#reference-list').append(this.createReference().$el);
      },
      'click #del-reference': function (e) {
        e.preventDefault();
        var id = $(e.target).attr('data-id');
        this._references[id].$el.remove();
        delete this._references[id];
      }
    },

    initialize: function (options) {
      this.templates = options.form.templateCollection;
      Backbone.Form.editors.Base.prototype.initialize.call(this, options);
      var tmpl = _.template(fileReferenceEditorTmpl, {});
      this.$el.append(tmpl);
    },

    render: function () {
      if (this.value) {
        for (var i = 0; i < this.value.length; i++) {
          var html = this.createReference(this.value[i]).$el;
          this.$el.find('#reference-list').append(html);
        }
      }

      return this;
    },

    getValue: function () {
      var result = [];
      for (var key in this._references) {
        result.push(this._references[key].getValue());
      }
      return result;
    },

    createReference: function (value) {
      var reference = new FileReferenceRow({templates: this.templates});
      reference._id = this.counter;

      if (value) {
        reference._template_id = value.template_id;
        reference._resource_id = value.resource_id;
        reference._description = value.description;
      }

      return this._references[this.counter++] = reference.render();
    }
  });

  return FileReferenceEditor;
});
