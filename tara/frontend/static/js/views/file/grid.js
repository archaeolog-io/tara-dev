define(['backgrid', 'translator'], function (Backgrid, T) {
  "use strict";

  var FileGrid = Backgrid.Grid.extend({
    initialize: function (options) {
      options.columns = [
        {
          name: '_preview',
          label: T('file-preview'),
          editable: false,
          cell: Backgrid.Cell.extend({
            render: function () {
              if(this.model.get('contentType') == 'image/jpeg') {
                var image = '<a href="/files/<%= _id %>/edit">' + '<img src="/api/download/<%= _id %>?w=400&h=400" />' + '</a>';
                this.$el.html(_.template(image, this.model.attributes));
              }
              return this;
            }
          })
        },
        {
          name: 'title',
          label: T('file-title'),
          editable: false,
          cell: 'string'
        },
        {
          name: 'contentType',
          label: T('file-type'),
          editable: false,
          cell: 'string'
        },
        {
          name: 'uploadDate',
          label: T('file-date'),
          editable: false,
          cell: 'string',
          formatter: {
            fromRaw: function (value) {
              return new Date(value).toLocaleString();
            }
          }
        },
        {
          name: '_action',
          label: T('file-action'),
          editable: false,
          cell: Backgrid.Cell.extend({
            render: function () {
              var edit_btn = '<a href="/files/<%= _id %>/edit" class="btn">' + T('button-edit') + '</a>&nbsp;';
              this.$el.html(_.template(edit_btn, this.model.attributes));
              return this;
            }
          })
        }
      ];
      this.constructor.__super__.initialize.apply(this, [options]);
    }
  });

  return FileGrid;
});
