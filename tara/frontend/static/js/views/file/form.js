define(['views/file/reference_editor', 'collections/classificator', 'translator', 'backbone-forms'], function (FileReferenceEditor, ClassificatorCollection, T) {
  "use strict";

  var FileForm = Backbone.Form.extend({
    schema: {
      title: {
        type: 'Text',
        title: T('file-title'),
        validators: ['required']
      },
      author: {
        type: 'Text',
        title: T('file-author')
      },
      description: {
        type: 'TextArea',
        title: T('file-description')
      },
      notes: {
        type: 'TextArea',
        title: T('file-notes')
      },
      categories: {
        type: 'Checkboxes',
        title: T('file-categories'),
        options: function (callback) {
          var classificatorCollection = new ClassificatorCollection();
          classificatorCollection.fetch({data: {type: 'FILE_CATEGORIES'}, success: function () {
            var result = _.map(classificatorCollection.toJSON(), function (elem) { return {val: elem.value, label: elem.name} });
            callback(result);
          }});
        }
      },
      references: {
        title: T('file-references'),
        type: FileReferenceEditor
      }
    },

    initialize: function (options) {
      this.templateCollection = options.templateCollection;
      Backbone.Form.prototype.initialize.call(this, options);
    }
  });

  return FileForm;
});
