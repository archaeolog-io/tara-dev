define(['backbone', 'views/file/reference_editor', 'models/file', 'text!/static/templates/file/add.html', 'translator'], function (Backbone, FileReferenceEditor, FileModel, fileAddTmpl, T) {
  "use strict";


  var FileAddView = Backbone.View.extend({
    events: {
      'change #file-selector': function (e) {
        this.files = e.target.files;

        var tbody_el = this.$el.find('#my-table > tbody');
        tbody_el.empty();

        var self = this;
        _.each(this.files, function (file) {
          tbody_el.append('<tr><td>'+file.name+'</td><td>'+file.size+'</td><td>'+file.type+'</td></tr>');
        });

        // enable save button
        this.$el.find('#save-item').prop('disabled', false);
      },
      'click #save-item': function (e) {
        e.preventDefault();

         // disable save button
        this.$el.find('#save-item').prop('disabled', true);

        // display progressbar
        this.$el.find('.progress').show();
        this.$el.find('.progress').find('.bar').css('width', '1%');

        var self = this;
        var counter = 0;

        var selector = this.$el.find('#my-table > tbody td:first-child');

        _.each(this.files, function (file) {
          var formData = new FormData();
          formData.append('file', file);

          $.ajax({
            url: '/api/files/',
            type: 'post',
            data: formData,
            processData: false,
            contentType: false,
            success: function (resp) {
              // Update references
              var fileModel = new FileModel({_id: resp._id});
              fileModel.set('references', self.references.getValue());

              if(++counter == self.files.length) {
                // notify/annoy user for successfull upload
                $(document).find('#player')[0].play();

                // hide progress-bar
                self.$el.find('.progress').hide();
                Backbone.Message.send_success(T('file-upload-success'));
              }

              var percent = parseInt((counter / self.files.length) * 100);
              self.$el.find('.progress').find('.bar').css('width', percent + '%');

//              selector.filter(function () {
//                return $(this).text() === resp.filename;
//              }).parent().eq(0).css({'background-color': 'green'});
            }
          });
        });

      }
    },

    initialize: function (options) {
      this.templateCollection  = options.templateCollection;
      this.params = options.params;
      this.references = new FileReferenceEditor({form: options});
    },

    render: function () {
      if (this.params) {
        if (this.params.template_id &&  this.params.resource_id) {
          this.references.value = [{
            template_id: this.params.template_id,
            resource_id: this.params.resource_id
          }];
        }
      }

      var html = _.template(fileAddTmpl);
      this.$el.append(html);
      this.$el.find('#references').append(this.references.render().$el);

      // hide progress-bar
      this.$el.find('.progress').hide();

      return this;
    }
  });

  return FileAddView;
});
