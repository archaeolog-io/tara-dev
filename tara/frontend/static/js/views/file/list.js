  define(['collections/file', 'collections/resource', 'collections/classificator', 'views/file/grid', 'text!/static/templates/file/list.html', 'backgrid-paginator'],
function (FileCollection, ResourceCollection, ClassificatorCollection, FileGrid, fileListTmpl) {
  "use strict";

  var FileListView = Backbone.View.extend({
    search: {
      title: 'Pealkiri',
      author: 'Autor',
      description: 'Kirjeldus',
      notes: 'Märkused',
     'references.template_id': 'Templeidi ID',
     'references.resource_id': 'Kirje ID'
    },

    template: _.template(fileListTmpl),

    events: {
      'submit form': function (e) {
        var self = this;
        e.preventDefault();

        var key = e.target[1].value;
        var value = e.target[2].value;
        var params = self.collection.queryParams;

        if (key && value) {
          if (params[key]) {
            if (!_.isArray(params[key])) {
              params[key] = [params[key]];
            }
            if (_.indexOf(params[key], value) == -1) {
              params[key].push(value);
            }
          } else {
            params[key] = value;
          }
          this.collection.fetch();
        }
      },

      'change #category-filter': function (e) {
        var value = $(e.target).val();
        if (value) {
          this.collection.queryParams['categories'] = value;
        } else {
          delete this.collection.queryParams['categories'];
        }
        this.collection.fetch();
      },

      /* Remove filter */
      'click span[class="label"]': function (e) {
        var key = $(e.target).attr('data-key');
        var val  = $(e.target).attr('data-val');
        var params = this.collection.queryParams;

        if (_.isArray(params[key])) {
          var pos = _.indexOf(params[key], val);
          if (pos != -1) {
            params[key].splice(pos, 1);
          }
        } else {
          delete params[key];
        }

        this.collection.fetch();
      }
    },

    initialize: function (options) {
      this.collection = new FileCollection();
      this.reserved_qp = _.clone(this.collection.queryParams);

      // set sorting
      if (options.params && _.has(options.params, 'sort_by') && _.has(options.params, 'order')) {
        this._sort_by = options.params['sort_by'];
        this._order = options.params['order'];
        delete options.params['sort_by'];
        delete options.params['order'];
      }

      // set page
      if (options.params && _.has(options.params, 'page')) {
        this.collection.state['currentPage'] = parseInt(options.params['page']);
        delete options.params['page'];
      }

      // set query params
      var self = this;
      _.each(options.params, function (val, key) {
        if (_.isObject(val)) {
          _.each(val, function(xval, xkey) {
            self.collection.queryParams[key + '.' + xkey] = xval;
          });
        } else {
          self.collection.queryParams[key] = val;
        }
      });

      // event used for hidding or displaying paginator, depending on page count
      this.listenTo(this.collection, 'sync', this.after_sync);

      this.categories = new ClassificatorCollection();
      this.listenTo(this.categories, 'reset', this.render_category_filter);
    },

    after_sync: function (collection) {
      var self = this;
      var url = Backbone.history.fragment;

      // clear url query params
      if (_.contains(url, '?')) {
        url = url.split('?')[0];
      }

      // starting of query params
      url += '?';

      $('#filter-list').empty();

      var filter_generator = function (key, value, label) {
        var str = _.str.sprintf('<span><span class="label" data-key="%1$s" data-val="%2$s">%3$s = %2$s</span>&nbsp;&nbsp;</span>', key, value, label);
        return $(str);
      };

      _.each(this.collection.queryParams, function (val, key) {
        if(!_.has(self.reserved_qp, key)) {
          if (_.isArray(val)) {
            _.each(val, function (xval) {
              url += key + '=' + xval + '&';
            });
          }
          else if (_.isObject(val)) {
            _.each(val, function (xval, xkey) {
              url += key + '.' + xkey + '=' + xval + '&';
            });
          } else {
            url += key + '=' + val + '&';
          }
          if (key != 'categories') {
            if (_.isArray(val)) {
              _.each(val, function (xval) {
                var html = filter_generator(key, xval, self.search[key]);
                self.$el.find('#filter-list').append(html);
              });
            }
            var html = filter_generator(key, val, self.search[key]);
            self.$el.find('#filter-list').append(html);
          }
        }
      });

      // add sort by query param
      if (this.collection.state['sortKey']) {
        url += 'sort_by' + '=' + this.collection.state['sortKey'] + '&';

        var order = this.collection.state['order'] == -1 ? 'ascending' : 'descending';
        url += 'order' + '=' + order + '&';
      }

      // add current page
      if (this.collection.state['currentPage'] && this.collection.state['currentPage'] != 1) {
        url += 'page' + '=' + this.collection.state['currentPage'] + '&';
      }

      // remove last &
      url = url.substring(0, url.length - 1);

      // set new url if currnet url ne composed url
      if (decodeURIComponent(Backbone.history.fragment) != url) {
        Backbone.history.navigate(url);
      }

      // show or hide paginator depending on page count
      if(collection.state.totalPages < 2) {
        this.$el.find('#grid-content').find('.backgrid-paginator').hide();
      } else {
        this.$el.find('#grid-content').find('.backgrid-paginator').show();
      }

      // show user total count of items in collection
      this.$el.find('#total-count').show();
      this.$el.find('#total-count-nr').html(collection.state.totalRecords);
    },

    render_category_filter: function (categories) {
      var options = this.$el.find('#category-filter');
      categories.forEach(function (model) {
        options.append($('<option />').val(model.get('value')).text(model.get('name')));
      });

      // set selectbox value
      var val = this.collection.queryParams['categories'];
      if (val) {
        this.$el.find('#category-filter').val(val);
      }
    },

    render: function () {
      this.$el.append(this.template({search: this.search}));

      // render grid
      this.grid = new FileGrid({collection: this.collection});
      this.$el.find('#grid-content').append(this.grid.render().$el);

      // sort grid
      if (this._sort_by && this._order) {
        var self = this;
        _.each(this.grid.header.row.cells, function (item) {
          if (item.column.attributes.name == self._sort_by) {
            item.sort(self._sort_by, self._order);
          }
        });
      }

      // render paginator
      this.paginator = new Backgrid.Extension.Paginator({collection: this.collection});
      this.$el.find('#grid-content').append(this.paginator.render().$el);

      this.collection.fetch();
      this.categories.fetch({reset: true, data: {type: 'FILE_CATEGORIES'}});

      return this;
    },

    onClose: function () {
      this.paginator.remove();
      this.grid.remove();
    }
  });

  return FileListView;
});
