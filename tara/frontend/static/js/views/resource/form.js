define(['backbone', 'helpers/input-autocomplete', 'helpers/select-autocomplete', 'helpers/metadata', 'collections/classificator'],
function (Backbone, InputAutocompleteEditor, SelectAutocompleteEditor, MetadataEditor, ClassificatorCollection) {
  "use strict";

  var ResourceForm = Backbone.Form.extend({
    initialize: function (options) {
      options.schema  = {};


      _.each(options.form_fields, function (item) {

        if (item.type == 'text') {
          if (item.autocomplete) {
            options.schema[item.name] = {title: item.label, type: InputAutocompleteEditor, autocomplete: item.autocomplete};
          } else {
            options.schema[item.name] = {title: item.label, type: 'Text'};
          }
        }
        else if (item.type == 'select') {

          if (item.autocomplete) {
            options.schema[item.name] = {title: item.label, type: SelectAutocompleteEditor, autocomplete: item.autocomplete, options: []};
          } else {
            options.schema[item.name] = {title: item.label, type: 'Select', options: []};
          }
        }
        else if (item.type == 'textarea') {
          options.schema[item.name] = {title: item.label, type: 'TextArea'};
        }
        else if (item.type == 'number') {
          options.schema[item.name] = {title: item.label, type: 'Number'};
        }
        else if (item.type == 'metadata') {
          options.schema[item.name] = {title: item.label, type: MetadataEditor};
        }
        else if (item.type == 'classificator') {
          options.schema[item.name] = {title: item.label, type: 'Select', options: function (callback) {
            callback([{val: '', label: '--'}]);
            var classificatorCollection = new ClassificatorCollection();
            classificatorCollection.fetch({data: {type: item.where}, success: function () {
              var result = [{val: '', label: '--'}];
              classificatorCollection.forEach(function (elem) {
                result.push({val: elem.attributes.value, label: elem.attributes.name});
              });
              callback(result);
            }});
          }};
        }
        else if (item.type == 'classificator_multi') {
          options.schema[item.name] = {title: item.label, type: 'Checkboxes', options: function (callback) {
            callback([{val: '', label: '--'}]);
            var classificatorCollection = new ClassificatorCollection();
            classificatorCollection.fetch({data: {type: item.where}, success: function () {
              var result = [];
              classificatorCollection.forEach(function (elem) {
                result.push({val: elem.attributes.value, label: elem.attributes.name});
              });
              callback(result);
            }});
          }};
        }

        if (item.required) {
          options.schema[item.name].validators = ['required'];
        }
      });

      delete options.form_fields;
      Backbone.Form.prototype.initialize.call(this, options);
    }
  });

  return ResourceForm;
});
