define(['backbone', 'text!/static/templates/resource/widgets/detail.html'], function (Backbone, detailWidgetTmpl) {
  "use strict";

  var DetailWidgetView = Backbone.View.extend({
    events: {
      'click .panel-title': function () {
        var key = '_widget_detail';
        if (localStorage.getItem(key) && localStorage.getItem(key) == 'true') {
          localStorage.setItem(key, false);
        } else {
          localStorage.setItem(key, true);
        }
      }
    },

    initialize: function (options) {
      this.template = options.template;
      this.model = options.model;
    },

    render: function () {
      var self = this;
      var result = [];

      _.each(this.template.grid_fields, function (field) {
          result.push({
            label: field.label,
            value: self.model[field.name]
          });
      });

      var tmpl = _.template(detailWidgetTmpl, {template: this.template, resourceId: this.model._id, result: result});
      this.$el.append(tmpl);

      var key = '_widget_detail';
      if (localStorage.getItem(key) && localStorage.getItem(key) == 'true') {
        window.setTimeout(function () {
          self.$el.find('.collapse').collapse('show');
        }, 10);
      }

      return this;
    }
  });

  return DetailWidgetView;
});
