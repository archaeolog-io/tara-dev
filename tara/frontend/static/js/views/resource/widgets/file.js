define(['backbone', 'collections/file', 'text!/static/templates/resource/widgets/file.html'], function (Backbone, FileCollection, fileWidgetTmpl) {
  "use strict";

  var FileWidgetView = Backbone.View.extend({
    events: {
      'click .panel-title': function () {
        var key = '_widget_file';
        if (localStorage.getItem(key) && localStorage.getItem(key) == 'true') {
          localStorage.setItem(key, false);
        } else {
          localStorage.setItem(key, true);
        }
      }
    },

    initialize: function (options) {
      this.templateId = options.templateId;
      this.resourceId = options.resourceId;

      this.collection = new FileCollection([], {
        state: {
          pageSize: 6
        },
        queryParams: {
          'references.resource_id': options.resourceId
        }
      });
    },

    render: function () {
      var self = this;

      this.collection.fetch({
        success: function () {
          var tmpl = _.template(fileWidgetTmpl, {files: self.collection, templateId: self.templateId, resourceId: self.resourceId});
          self.$el.append(tmpl);
          var key = '_widget_file';
          if (localStorage.getItem(key) && localStorage.getItem(key) == 'true') {
            self.$el.find('.collapse').collapse('show');
          }
        }
      });

      return this;
    }
  });

  return FileWidgetView;
});
