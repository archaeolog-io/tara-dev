define(['backbone', 'proj4', 'text!/static/templates/resource/widgets/map.html', 'goog!maps,3,other_params:sensor=false'], function (Backbone, Proj4js, mapWidgetTtmpl) {
  "use strict";

  var MapWidgetView = Backbone.View.extend({
    events: {
      'click .panel-title': function () {
        var key = '_widget_map';
        if (localStorage.getItem(key) && localStorage.getItem(key) == 'true') {
          localStorage.setItem(key, false);
        } else {
          localStorage.setItem(key, true);
        }
      }
    },

    initialize: function (options) {
      if (options.coord_x && options.coord_y) {
        var src = new Proj4js.Proj("EPSG:3301");
        var dst = new Proj4js.Proj("EPSG:4326");

        var point = new Proj4js.Point(options.coord_y, options.coord_x);

        Proj4js.transform(src, dst, point);

        this.height = options.height;
        this.coords = new google.maps.LatLng(point.y, point.x);
      }
    },

    render: function () {
      if (this.coords) {
        this.$el.append(mapWidgetTtmpl);
        var what = this.$el.find('#map-body');

        var mapOptions = {
          center: this.coords,
          zoom: 12,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        var map = new google.maps.Map(what[0], mapOptions);

        new google.maps.Marker({map: map, position: this.coords});

        google.maps.event.addListener(map, 'idle', function(){
            map.setCenter(mapOptions.center);
            google.maps.event.trigger(map, 'resize');
        });

        what.css('height', this.height);

        var self = this;

        var key = '_widget_map';
        if (localStorage.getItem(key) && localStorage.getItem(key) == 'true') {
          window.setTimeout(function () {
            self.$el.find('.collapse').collapse('show');
            console.log('collapse')
          }, 10);
        }
      }

      return this;
    }
  });

  return MapWidgetView;
});
