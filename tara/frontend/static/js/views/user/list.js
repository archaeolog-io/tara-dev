define(['collections/user', 'views/user/grid', 'text!/static/templates/user/list.html'], function (UserCollection, UserGrid, userListTmpl) {
  "use strict";

  var UserListView = Backbone.View.extend({
    template: _.template(userListTmpl),

    initialize: function () {
      this.collection = new UserCollection();
      this.grid = new UserGrid({collection: this.collection});
    },

    render: function () {
      this.$el.append(this.template({permissions: Backbone.View.permissions()}));
      this.$el.find('#grid-content').append(this.grid.render().$el);
      this.collection.fetch({reset: true});
      return this;
    },

    onClose: function () {
      this.grid.remove();
    }
  });

  return UserListView;
});
