define(['backbone', 'text!/static/templates/settings/index.html'], function (Backbone, settingsTmpl) {
  "use strict";

  var SettingsView = Backbone.View.extend({
    template: _.template(settingsTmpl),

    render: function () {
      this.$el.append(this.template);
      return this;
    }
  });

  return SettingsView;
});
