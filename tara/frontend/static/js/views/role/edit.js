define(['backbone', 'views/role/form', 'models/role', 'text!/static/templates/role/edit.html', 'translator'], function (Backbone, RoleForm, RoleModel, roleEditTmpl, T) {
  "use strict";

  var RoleEditView = Backbone.View.extend({
    template: _.template(roleEditTmpl),

    events: {
      'click #save-item': function (e) {
        e.preventDefault();
        this.form.commit();
      },
      'click #delete-item': function (e) {
        e.preventDefault();
        if(confirm(T('confirm-delete'))) {
          this.model.destroy({success: function () {
            Backbone.history.navigate('roles', {trigger: true});
          }});
        }
      }
    },

    initialize: function (options) {
      this.model = new RoleModel({_id: options.roleId});
      this.listenTo(this.model, 'sync', this.redirect);
    },

    render: function () {
      var self = this;
      this.$el.append(this.template({permissions: Backbone.View.permissions()}));

      this.model.fetch({save: false, success: function () {
        var tmpl = _.template('<div data-fields="*"></div>');
        self.form = new RoleForm({template: tmpl, model: self.model});
        self.$el.find('#form-content').append(self.form.render().el);
      }});
      return this;
    },

    redirect: function (model, resp, options) {
      if (options.patch) {
        Backbone.history.navigate('roles', {trigger: true});
      }
    },

    onClose: function () {
      if (this.form) {
        this.form.remove();
      }
    }
  });

  return RoleEditView;
});
