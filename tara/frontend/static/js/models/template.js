define(['backbone', 'backbone-forms'], function (Backbone) {
  "use strict";

  var TemplateModel = Backbone.Model.extend({
    idAttribute: '_id',

    urlRoot: '/api/templates/',

    defaults: {
      parent: null
    },

    initialize: function () {
      Backbone.Model.prototype.initialize.apply(this, arguments);
      this.on('change', function (model, options) {
        if (options && options.save === false) {
          return;
        }
        if (model.changed._id) {
          return;
        }
        model.save(model.changed, {patch: true});
      });
    },

    toString: function () {
      return this.get('label');
    }
  });

  return TemplateModel;
});
