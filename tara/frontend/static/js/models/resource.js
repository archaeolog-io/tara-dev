define(['backbone'], function (Backbone) {
  "use strict";

  var ResourceModel = Backbone.Model.extend({
    idAttribute: '_id',

    urlRoot: function () {
      var templateId = this.templateId || this.collection.templateId;
      return '/api/resources/'+templateId+'/'
    },

    initialize: function (attributes, options1) {
      this.templateId = options1.templateId;

      Backbone.Model.prototype.initialize.apply(this, arguments);
      this.on('change', function (model, options2) {
        if (options2 && options2.save === false) {
          return;
        }
        if (model.changed._id || model.changed._updated) {
          return;
        }
        return model.save(model.changed, {patch: true});
      });
    }
  });

  return ResourceModel;
});
