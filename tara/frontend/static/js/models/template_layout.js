define(['backbone'], function (Backbone) {
  "use strict";

  var TemplateLayoutModel = Backbone.Model.extend({
    idAttribute: '_id',

    urlRoot: '/api/templates/layouts/',

    initialize: function () {
      Backbone.Model.prototype.initialize.apply(this, arguments);
      this.on('change', function (model, options) {
        if (options && options.save === false) {
          return;
        }
        if (model.changed._id) {
          return;
        }
        model.save(model.changed, {patch: true});
      });
    }

  });

  return TemplateLayoutModel;
});
