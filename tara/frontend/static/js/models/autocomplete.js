define(['backbone'], function (Backbone) {
  "use strict";

  var AutocompleteModel = Backbone.Model.extend({
    initialize: function (attr, options) {
      this.options = options;
    },

    url: function () {
      return '/api/autocomplete/' + this.options.templateId + '/'+ this.options.field;
    }
  });

  return AutocompleteModel;
});
