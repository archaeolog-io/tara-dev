from tara import factory

from functools import wraps
from flask import request, redirect, current_app, render_template


def ssl_required(fn):
    @wraps(fn)
    def decorated_view(*args, **kwargs):
        if current_app.config.get("SSL"):
            if request.is_secure:
                return fn(*args, **kwargs)
            else:
                return redirect(request.url.replace("http://", "https://"))
        return fn(*args, **kwargs)
    return decorated_view


def create_app(settings_override=None):
    """Returns the API application instance"""
    app = factory.create_app(__name__, settings_override)

    @app.route('/not_supported')
    def not_supported():
        return render_template('not_supported.html')

    @app.route('/login/id_callback', methods=['POST'])
    def id_callback():
        user = request.form.get('user')
        grant = request.form.get('grant')
        return render_template('id_auth.html', user=user, grant=grant)

    @app.route('/', defaults={'path': ''})
    @app.route('/<path:path>')
    def index(path):
        return '''<!DOCTYPE html>
<html>
<head>
<title>TARA</title>
<link rel="shortcut icon" href="/static/img/favicon.ico" />
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400,300">
<link rel="stylesheet" href="/static/css/bootstrap.css">
<link rel="stylesheet" href="/static/css/backgrid.css">
<link rel="stylesheet" href="/static/css/backgrid-paginator.css">
<link rel="stylesheet" href="/static/css/custom.css">
<link rel="stylesheet" href="/static/css/default.css">
<link rel="stylesheet" href="/static/css/forms.css">
<link rel="stylesheet" href="/static/css/print.css">
<script data-main="/static/js/main" src="/static/js/libs/require/require.js"></script>
<meta charset="utf-8">
</head>
<body>
</body>
</html>
'''
    return app
