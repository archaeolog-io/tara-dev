from datetime import datetime, timedelta
from flask import jsonify, request

from tara.core import mongo, login_manager


def configure_auth(app):
    @login_manager.user_loader
    def load_user():
        grant = request.headers.get('X-Tara-Grant')
        if grant:
            timestamp = datetime.utcnow() - timedelta(hours=1)
            grant_obj = mongo.db.grant.find_one({'grant': grant, 'timestamp': {'$gt': timestamp}})
            if grant_obj:
                mongo.db.grant.update(grant_obj, {'$set': {'timestamp': datetime.utcnow()}})
                user = mongo.db.users.find_one(grant_obj['user'])
                user['full_name'] = user['first_name'] + ' ' + user['last_name']
                return user

    @login_manager.unauthenticated_handle
    def user_unauthenticated():
        response = jsonify(error='Not authenticated')
        response.status_code = 401
        return response


    @login_manager.unauthorized_handle
    def user_unauthorized():
        response = jsonify(error='Not authorized')
        response.status_code = 403
        return response
