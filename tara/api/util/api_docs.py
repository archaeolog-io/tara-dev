from flask import render_template


def configure_docs(app):
    @app.route('/')
    def index():
        rules = app.url_map.iter_rules()
        url_map = sorted(rules, key=lambda url: url.rule)
        return render_template('index.html', url_map=url_map)

    @app.template_filter('sorted')
    def sorted_filter(s):
        l = []
        for item in s:
            l.append(item)
        return sorted(l)

    @app.template_filter('remove_head')
    def remove_head_filter(s):
        if 'HEAD' in s:
            s.remove('HEAD')
        return s
