ADMINS = ['sander@wiseman.ee']


def configure_logging(app):
    if not app.debug:
        import logging
        from logging.handlers import SMTPHandler
        mail_handler = SMTPHandler('127.0.0.1', 'no-replay@tara.ut.ee', ADMINS, 'TARA Failed')
        mail_handler.setLevel(logging.ERROR)
        app.logger.addHandler(mail_handler)
