from bson.errors import InvalidId
from flask import jsonify
from schema import SchemaError

from tara.api.service.exceptions import ResourceNotFound, SearchQueryError, InsertEmptyRecordError, UniqueFieldError, OldPasswordMismatch


def schema_error(e):
    resp = jsonify(
        error='Schema validation error',
        code=100,
        details=e.autos
    )
    resp.status_code = 400
    return resp


def resource_not_found(e):
    resp = jsonify(
        error='Resource not found',
        code=101
    )
    resp.status_code = 404
    return resp


def search_query_error(e):
    resp = jsonify(
        error='Search query error',
        code=102
    )
    resp.status_code = 400
    return resp


def invalid_id(e):
    resp = jsonify(
        error='ObjectId is not valid',
        code=103
    )
    resp.status_code = 400
    return resp


def insert_empty_record(e):
    resp = jsonify(
        error='Cannot insert empty record',
        code=104
    )
    resp.status_code = 400
    return resp


def insert_non_unique_recorde(e):
    resp = jsonify(
        error='Importing non-unique field error',
        code=105
    )
    resp.status_code = 400
    return resp


def change_password_wrong_pw(e):
    resp = jsonify(
        error='Old password mismatch',
        code=106
    )
    resp.status_code = 400
    return resp


def init_error_handlers(app):
    app.register_error_handler(SchemaError, schema_error)
    app.register_error_handler(ResourceNotFound, resource_not_found)
    app.register_error_handler(SearchQueryError, search_query_error)
    app.register_error_handler(InvalidId, invalid_id)
    app.register_error_handler(InsertEmptyRecordError, insert_empty_record)
    app.register_error_handler(UniqueFieldError, insert_non_unique_recorde)
    app.register_error_handler(OldPasswordMismatch, change_password_wrong_pw)
