import json
from flask import Response
from werkzeug.routing import BaseConverter
from werkzeug.exceptions import HTTPException
from itsdangerous import base64_encode
from bson.objectid import ObjectId
from schema import Schema, And, Use, SchemaError


class ObjectIDConverter(BaseConverter):
    def to_python(self, value):
        schema = Schema(And(ObjectId.is_valid, Use(ObjectId)))
        try:
            return schema.validate(value)
        except SchemaError:
            json_dump = json.dumps(dict(
                error='Invalid ObjectId'
            ))
            resp = Response(json_dump, mimetype='application/json')
            resp.status_code = 400
            raise HTTPException(response=resp)

    def to_url(self, value):
        return base64_encode(value.binary)
