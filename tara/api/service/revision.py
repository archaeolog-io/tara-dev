from bson.objectid import ObjectId
from datetime import datetime
from schema import Schema
from tara.core import mongo
from tara.restful_login import current_user


class RevisionService():
    def find(self, template_id, resource_id):
        cursor = mongo.db.revisions.find({'template_id': template_id, 'resource_id': resource_id})
        cursor.sort('_inserted', -1)
        return dict(result=cursor)

    def insert(self, data):
        document = Schema(dict(
            template_id=ObjectId,
            resource_id=ObjectId,
            old=dict,
            new=dict
        )).validate(data)

        document['_inserted'] = datetime.utcnow()

        if current_user:
            document['_creator_id'] = current_user['_id']
            document['_creator_name'] = current_user['full_name']

        return mongo.db.revisions.insert(document)
