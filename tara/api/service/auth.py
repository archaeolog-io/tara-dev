# -*- coding: utf-8 -*-

from datetime import datetime, timedelta
from bson import ObjectId
from os import urandom

from tara.core import mongo, bcrypt

# Preauthentication token
REQUEST_TOKEN_EXPIRE_IN_SECONDS = 60


class AuthService():
    def __init__(self):
        pass

    def request_token(self):
        token = urandom(24).encode('hex')
        mongo.db.token.insert({'token': token})
        return token

    def authenticate(self, token, username, password):
        if not token:
            return dict(error='Missing token')

        if not username or not password:
            return dict(error='Viga, palun sisesta kasutajatunnus ja parool')

        timestamp = ObjectId.from_datetime(datetime.utcnow() - timedelta(seconds=REQUEST_TOKEN_EXPIRE_IN_SECONDS))
        token_obj = mongo.db.token.find_one({'token': token, '_id': {'$gt': timestamp}})
        if not token_obj:
            return dict(error='Invalid or expired token')

        # remove auth token
        mongo.db.token.remove(token_obj)

        # find user and validate password
        user_obj = mongo.db.users.find_one({'username': username})
        if user_obj and user_obj['is_active'] and bcrypt.check_password_hash(user_obj['password'], password):
            # remove old grants
            if username != 'anonymous':
                mongo.db.grant.remove({'user': user_obj['_id']})

            # create greant, store db, return grant
            grant = urandom(24).encode('hex')
            mongo.db.grant.insert(dict(
                grant=grant,
                user=user_obj['_id'],
                timestamp=datetime.utcnow()
            ))

            return dict(grant=grant, user=user_obj['_id'])

        return dict(error='Autentimine ebaõnnestus, vale kasutajatunnus või parool.')

    def logout(self, user_id):
        mongo.db.grant.remove({'user': user_id})
