import re

from datetime import datetime

from schema import Schema, Optional, And, Or, Use
from tara.core import mongo
from tara.restful_login import current_user
from tara.api.service.revision import RevisionService
from tara.api.service.exceptions import ResourceNotFound, SearchQueryError, InsertEmptyRecordError

RESERVED_ARGS = ['page', 'per_page', 'sort_by', 'order', 'total_pages', 'total_entries']


class ResourceService():
    def __init__(self):
        self.revision_service = RevisionService()

    def collection(self, template_id):
        template = mongo.db.templates.find_one(template_id)

        if template is None:
            raise ResourceNotFound()

        coll = str(template['parent'] or template['_id'])
        return [mongo.db[coll], template]

    def find(self, template_id, _id, args):
        collection, template = self.collection(template_id)

        if _id is None:
            query = {'$and': []}

            try:
                for key, values in args.iterlists():
                    if key not in RESERVED_ARGS:
                        if len(key) > 2 and key[-2:] == '[]':
                            key = key[:-2]
                        if len(values) == 1 and len(values[0]):
                            if key == '_ALL_':
                                _or = []
                                for item in template['form_fields']:
                                    if item['type'] in ['text', 'textarea']:
                                        _or.append({item['name']: {'$regex': re.compile(values[0], re.IGNORECASE)}})
                                    elif item['type'] == 'number' and values[0].isdigit():
                                        _or.append({item['name']: int(values[0])})
                                query['$and'].append({'$or': _or})
                            else:
                                item = filter(lambda s: s['name'] == key, template['form_fields'])[0]
                                if item['type'] in ['text', 'textarea']:
                                    query[key] = {'$regex': re.compile(values[0], re.IGNORECASE)}
                                elif item['type'] == 'number' and values[0].isdigit():
                                    query[key] = int(values[0])

                        elif len(values) > 1:
                            if key == '_ALL_':
                                _or = []
                                for val in values:
                                    for item in template['form_fields']:
                                        if item['type'] in ['text', 'textarea']:
                                            _or.append({item['name']: {'$regex': re.compile(val, re.IGNORECASE)}})
                                        elif item['type'] == 'number' and val.isdigit():
                                            _or.append({item['name']: int(val)})
                                query['$and'].append({'$or': _or})
                            else:
                                item = filter(lambda s: s['name'] == key, template['form_fields'])[0]
                                if item['type'] in ['text', 'textarea']:
                                    res = map(lambda xval: {key: {'$regex': re.compile(xval, re.IGNORECASE)}}, values)
                                    query['$and'].append({'$or': res})
                                elif item['type'] == 'number':
                                    res = map(lambda xval: {key: int(xval)}, filter(lambda x: x.isdigit(), values))
                                    query['$and'].append({'$or': res})

            except re.error:
                raise SearchQueryError

            if len(query['$and']) == 0:
                del query['$and']

            if template['parent']:
                query['_template_id'] = template_id

            # filter out deleted records
            query['_deleted'] = {'$exists': False}

            # cursor
            cursor = collection.find(query)

            # sorting
            if args.get('sort_by'):
                sort_by = args.get('sort_by', type=str)
                order = args.get('order', type=int)
                cursor.sort(sort_by, order)

            # skipping
            if args.get('page'):
                page = args.get('page', type=int)
                per_page = args.get('per_page', type=int)
                cursor.skip((page-1) * per_page).limit(per_page)

            # return result
            return dict(result=cursor, total_entries=cursor.count())

        else:
            result = collection.find_one(_id)
            if result is None:
                raise ResourceNotFound()
            return result

    def insert(self, template_id, data):
        # Don't allow empty data
        if len(data) == 0:
            raise InsertEmptyRecordError

        collection, template = self.collection(template_id)

        schema = {}
        for item in template['form_fields']:
            required = True if 'required' in item and item['required'] is True else False
            is_text = item['type'] in ['text', 'textarea', 'classificator']
            is_numb = item['type'] == 'number'

            if is_text:
                if required:
                    schema[item['name']] = Or(And(basestring, len), And(Or(int, float), Use(str)))
                else:
                    schema[item['name']] = Or(basestring, And(Or(int, float), Use(str)))
            elif is_numb:
                schema[item['name']] = Use(lambda s: int(s) if isinstance(s, basestring) and s.isdigit() else s)
            else:
                if required:
                    schema[item['name']] = And(Or(list, dict), len)
                else:
                    schema[item['name']] = Or(list, dict)

        document = Schema(schema).validate(data)
        is_empty = True

        # trim whitspaces from left and right and check document
        for key in document:
            if isinstance(document[key], basestring):
                document[key] = document[key].strip()
            if isinstance(document[key], int):
                is_empty = False
            else:
                if len(document[key]):
                    is_empty = False

        if is_empty:
            raise InsertEmptyRecordError

        document['_template_id'] = template_id

        if current_user:
            document['_creator_id'] = current_user['_id']
            document['_creator_name'] = current_user['full_name']

        document['_inserted'] = datetime.utcnow()

        # insert document
        return collection.insert(document)

    def update(self, template_id, _id, data):
        # Don't allow empty data
        if len(data) == 0:
            raise InsertEmptyRecordError

        collection, template = self.collection(template_id)

        schema = {}
        for item in template['form_fields']:
            required = True if 'required' in item and item['required'] is True else False
            is_text = item['type'] in ['text', 'textarea', 'classificator']
            is_numb = item['type'] == 'number'

            if is_text:
                if required:
                    schema[Optional(item['name'])] = Or(And(basestring, len), And(Or(int, float), Use(str)))
                else:
                    schema[Optional(item['name'])] = Or(basestring, And(Or(int, float), Use(str)))
            elif is_numb:
                schema[Optional(item['name'])] = Use(lambda s: int(s) if isinstance(s, basestring) and s.isdigit() else s)
            else:
                if required:
                    schema[Optional(item['name'])] = And(Or(list, dict), len)
                else:
                    schema[Optional(item['name'])] = Or(list, dict)

        document = Schema(schema).validate(data)

        old_document = self.find(template_id, _id, None)
        filtered = {}

        # store old values to revision and
        # trim whitspaces from left and right in new document
        for key in document:
            filtered[key] = old_document[key] if key in old_document else None
            if isinstance(document[key], basestring):
                document[key] = document[key].strip()

        self.revision_service.insert(dict(template_id=template_id, resource_id=_id, old=filtered, new=document))

        # update user information
        if current_user:
            document['_editor_id'] = current_user['_id']
            document['_editor_name'] = current_user['full_name']

        document['_updated'] = datetime.utcnow()

        # update document
        collection.update({'_id': _id}, {'$set': document})

    def remove(self, template_id, _id):
        document = dict(_deleted=datetime.utcnow())

        if current_user:
            document['_deletor_id'] = current_user['_id']
            document['_deletor_name'] = current_user['full_name']

        self.collection(template_id)[0].update({'_id': _id}, {'$set': document})
