import bson

from schema import Schema, And, Or, Use, Optional

from tara.core import mongo
from tara.api.service.exceptions import ResourceNotFound

LABEL_SCHEMA = And(basestring, len)
SORT_SCHEMA = Or(None, int)
PARENT_SCHEMA = Or(None, And(basestring, lambda s: bson.ObjectId.is_valid(s), Use(lambda s: bson.ObjectId(s))))
LAYOUT_SCHEMA = PARENT_SCHEMA
UNIQUE_SCHEMA = [basestring]
SHOW_ALL_BUTTON_ENABLED = bool

GRID_FIELD_TYPES = ['string']
GRID_FIELDS_SCHEMA = [{
    'name': basestring,
    'label': basestring,
    'type': And(basestring, lambda s: s in GRID_FIELD_TYPES)
}]

FORM_FIELD_TYPES = ['text', 'textarea', 'number', 'metadata', 'classificator', 'classificator_multi']
FORM_FIELDS_SCHEMA = [{
    'name': basestring,
    'label': basestring,
    'type': And(basestring, lambda s: s in FORM_FIELD_TYPES),
    Optional('autocomplete'): {'template_id': basestring, 'field': basestring},
    Optional('required'): bool,
    Optional('where'): basestring
}]


class TemplateService():
    def __init__(self):
        self.collection = mongo.db.templates

    def find(self, _id=None):
        if _id is None:
            return dict(result=self.collection.find().sort('sort'))
        else:
            result = self.collection.find_one(_id)
            if result is None:
                raise ResourceNotFound()
            return result

    def insert(self, data):
        validator = {
            'label': LABEL_SCHEMA,
            'grid_fields': GRID_FIELDS_SCHEMA,
            'form_fields': FORM_FIELDS_SCHEMA,
            Optional('show_all_enabled'): SHOW_ALL_BUTTON_ENABLED,
            Optional('parent'): PARENT_SCHEMA,
            Optional('layout'): LAYOUT_SCHEMA,
            Optional('sort'): SORT_SCHEMA,
            Optional('unique'): UNIQUE_SCHEMA
        }

        document = Schema(validator).validate(data)
        return self.collection.insert(document)

    def update(self, _id, data):
        validator = {
            Optional('label'): LABEL_SCHEMA,
            Optional('grid_fields'): GRID_FIELDS_SCHEMA,
            Optional('form_fields'): FORM_FIELDS_SCHEMA,
            Optional('show_all_enabled'): SHOW_ALL_BUTTON_ENABLED,
            Optional('parent'): PARENT_SCHEMA,
            Optional('layout'): LAYOUT_SCHEMA,
            Optional('sort'): SORT_SCHEMA,
            Optional('unique'): UNIQUE_SCHEMA
        }

        document = Schema(validator).validate(data)
        self.collection.update({'_id': _id}, {'$set': document})

    def remove(self, _id):
        self.collection.remove(_id)
