from tara.api.service.resource import ResourceService


class AutocompleteService():
    def __init__(self):
        self.service = ResourceService()

    def find(self, template_id, field):
        collection, template = self.service.collection(template_id)

        query = {field: {"$ne": None}, '_deleted': {'$exists': False}}

        if template['parent']:
            query['_template_id'] = template_id

        return {'result': collection.find(query).distinct(field)}
