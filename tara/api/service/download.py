from gridfs import GridFS, NoFile

from tara.api.service.exceptions import ResourceNotFound
from tara.api.service.image import ImageService

from tara.core import mongo


class DownloadService():
    def __init__(self):
        self.storage = GridFS(mongo.db, 'fs')
        self.cache = GridFS(mongo.db, 'cache')

    def find(self, _id, args):
        try:
            filedata = self.storage.get(_id)
        except NoFile:
            raise ResourceNotFound

        width = args.get('w', type=int)
        height = args.get('h', type=int)

        # for images, we cache
        if width and height and filedata.content_type == 'image/jpeg':
            params = {'file_id': _id, 'width': width, 'height': height}
            result = mongo.db['cache'].files.find_one(params)
            cached_id = result['_id'] if result else None

            if not cached_id:
                resized = ImageService.resize(filedata, width, height)

                params['contentType'] = filedata.content_type
                cached_id = self.cache.put(resized, **params)

            return self.cache.get(cached_id)
        else:
            return filedata
