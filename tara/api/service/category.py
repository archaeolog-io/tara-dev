from datetime import datetime
from schema import Schema, And, Or, Optional
from tara.core import mongo
from tara.restful_login import current_user
from tara.api.service.exceptions import ResourceNotFound

NAME_SCHEMA = And(basestring, len)
URL_SCHEMA = And(basestring, len)
IS_EXT_SCHEMA = bool
ICON_SCHEMA = And(basestring)
SORT_SCHEMA = Or(None, int)


class CategoryService():
    def __init__(self):
        self.categories = mongo.db.categories

    def find(self, _id=None):
        if _id is None:
            return dict(result=self.categories.find().sort('sort'))
        else:
            result = self.categories.find_one(_id)
            if result is None:
                raise ResourceNotFound()
            return result

    def insert(self, data):
        document = Schema({
            'name': NAME_SCHEMA,
            'url': URL_SCHEMA,
            'is_external': IS_EXT_SCHEMA,
            Optional('icon'): ICON_SCHEMA,
            Optional('sort'): SORT_SCHEMA
        }).validate(data)

        if current_user:
            document['_creator_id'] = current_user['_id']
            document['_creator_name'] = current_user['full_name']

        document['_inserted'] = datetime.utcnow()

        return self.categories.insert(document)

    def update(self, _id, data):
        document = Schema({
            Optional('name'): NAME_SCHEMA,
            Optional('url'): URL_SCHEMA,
            Optional('is_external'): IS_EXT_SCHEMA,
            Optional('icon'): ICON_SCHEMA,
            Optional('sort'): SORT_SCHEMA
        }).validate(data)

        if current_user:
            document['_editor_id'] = current_user['_id']
            document['_editor_name'] = current_user['full_name']

        document['_updated'] = datetime.utcnow()

        self.categories.update(dict(_id=_id), {'$set': document})

    def remove(self, _id):
        self.categories.remove(_id)
