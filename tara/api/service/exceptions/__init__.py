class ResourceNotFound(Exception):
    pass


class SearchQueryError(Exception):
    pass


class InsertEmptyRecordError(Exception):
    pass


class UniqueFieldError(Exception):
    pass


class OldPasswordMismatch(Exception):
    pass
