from PIL import Image
from StringIO import StringIO


class ImageService():
    @staticmethod
    def get_thumb_fillin_xy(image_x, image_y, rect_x, rect_y):
        image_x = float(image_x)
        image_y = float(image_y)
        rect_x = float(rect_x)
        rect_y = float(rect_y)

        # scalefactor == 1 - ruudukujuline pilt
        # scalefactor > 1  - horisontaalne pilt
        # scalefactor < 1  - vertikaalne pilt
        image_scalefactor = image_x / image_y  # originaali kylgede suhe
        rect_scalefactor = rect_x / rect_y  # thumbi platsi kylgede suhe

        # vertikaalne pilt
        if image_scalefactor < 1:
            if rect_scalefactor < image_scalefactor:  # (plats on veel kitsam kui pilt - alustame korgusest)
                scalefactor = rect_y / image_y
                thumb_x = scalefactor * image_x
                thumb_y = rect_y
            else:  # alustame laiusest
                scalefactor = rect_x / image_x
                thumb_x = rect_x
                thumb_y = scalefactor * image_y

        # horisontaalne pilt
        else:
            if rect_scalefactor < image_scalefactor:  # plats on veel kitsam kui pilt - alustame korgusest
                scalefactor = rect_y / image_y
                thumb_x = scalefactor * image_x
                thumb_y = rect_y
            else:  # alustame laiusest
                scalefactor = rect_x / image_x
                thumb_x = rect_x
                thumb_y = scalefactor * image_y

        # teeme taisarvuks need. kui on alla 1, siis paneme 1
        thumb_x = 1 if thumb_x < 1 else int(thumb_x)
        thumb_y = 1 if thumb_y < 1 else int(thumb_y)

        # kui algmaterjal on sellest alast vahemalt yhte kylgepidi vaiksem, siis jaavad pildi algmoodud
        if image_x < rect_x or image_y < rect_y:
            thumb_x = int(image_x)
            thumb_y = int(image_y)

        return thumb_x, thumb_y

    @staticmethod
    def resize(filedata, width, height, quality=80):
        image = Image.open(filedata)

        src_width, src_height = image.size
        dst_size = ImageService.get_thumb_fillin_xy(src_width, src_height, width, height)

        image = image.resize(dst_size, Image.BILINEAR)

        left = (dst_size[0] - width) / 2
        top = (dst_size[1] - height) / 2
        right = (dst_size[0] + width) / 2
        bottom = (dst_size[1] + height) / 2

        image = image.crop((left, top, right, bottom))

        io = StringIO()
        image.save(io, 'jpeg', quality=quality)

        io.seek(0)

        return io
