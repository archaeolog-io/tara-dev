from flask import Blueprint

from tara.api.controller import BaseMethodView, register_api
from tara.api.service.template_layout import TemplateLayoutService


class TemplateLayoutView(BaseMethodView):
    def __init__(self):
        self.service = TemplateLayoutService()

    def get(self, _id):
        return self.service.find(_id)

bp = Blueprint('template_layout', __name__, url_prefix='/templates/layouts')
register_api(bp, TemplateLayoutView)
