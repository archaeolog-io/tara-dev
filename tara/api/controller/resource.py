from flask import Blueprint, request

from tara.api.controller import BaseMethodView, register_api
from tara.api.service.resource import ResourceService

from tara.api.util.decorators import login_required


class ResourceView(BaseMethodView):
    def __init__(self):
        self.service = ResourceService()

    @login_required
    def get(self, template_id, _id):
        return self.service.find(template_id, _id, request.args)

    @login_required
    def post(self, template_id):
        data = request.get_json()
        _id = self.service.insert(template_id, data)
        return self.get(template_id, _id)

    @login_required
    def patch(self, template_id, _id):
        data = request.get_json()
        self.service.update(template_id, _id, data)
        return self.get(template_id, _id)

    @login_required
    def delete(self, template_id, _id):
        result = self.get(template_id, _id)
        self.service.remove(template_id, _id)
        return result


bp = Blueprint('resource', __name__, url_prefix='/resources')
register_api(bp, ResourceView, url_prefix='/<ObjectId:template_id>')
