from flask import Blueprint
from flask.views import MethodView

from tara.api.util.decorators import login_required, return_json
from tara.api.service.revision import RevisionService


class RevisionView(MethodView):
    decorators = [return_json]

    def __init__(self):
        self.service = RevisionService()

    @login_required
    def get(self, template_id, resource_id):
        return self.service.find(template_id, resource_id)


bp = Blueprint('revision', __name__, url_prefix='/revisions')

view_func = RevisionView.as_view(bp.name)
view_func.provide_automatic_options = False

bp.add_url_rule('/<ObjectId:template_id>/<ObjectId:resource_id>', view_func=view_func, methods=['GET'])
