import csv

from flask import Blueprint, request
from flask.views import MethodView
from tara.api.service.exceptions import UniqueFieldError
from tara.api.service.resource import ResourceService

from tara.api.util.decorators import login_required, return_json


class ImportView(MethodView):
    decorators = [return_json]

    def __init__(self):
        self.service = ResourceService()

    @login_required
    def post(self, template_id):
        resources, template = self.service.collection(template_id)

        tmpl_idx_by_name = {}
        for idx, item in enumerate(template['form_fields']):
            tmpl_idx_by_name[item['name']] = idx

        csv_file = request.files['file']
        skip_first_row = request.form.get('skip_first_row') == 'true'

        reader = csv.reader(csv_file)

        if skip_first_row:
            reader.next()

        # unique check/validation
        if 'unique' in template and len(template['unique']):
            for idx, row in enumerate(reader):
                query = {}
                for key in template['unique']:
                    query[key] = {'$regex': '^'+row[tmpl_idx_by_name[key]]+'$'}

                # filter out deleted records
                query['_deleted'] = {'$exists': False}

                if resources.find_one(query):
                    raise UniqueFieldError

        # goto beginning of the file
        csv_file.seek(0)
        reader = csv.reader(csv_file)

        if skip_first_row:
            reader.next()

        # insert rows
        for row in reader:
            doc = {}
            skip_row = True
            for idx, item in enumerate(template['form_fields']):
                if idx >= len(row):
                    doc[item['name']] = ''
                else:
                    # we dont want to insert empty records
                    # or actually we would get empty record exeception
                    if row[idx] != '':
                        skip_row = False
                    doc[item['name']] = row[idx]
            if not skip_row:
                self.service.insert(template_id, doc)
        return 'ok'


bp = Blueprint('import', __name__, url_prefix='/import')

view_func = ImportView.as_view(bp.name)
view_func.provide_automatic_options = False

bp.add_url_rule('/<ObjectId:template_id>', view_func=view_func, methods=['POST'])
