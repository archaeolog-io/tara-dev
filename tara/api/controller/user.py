from flask import Blueprint, request
from flask.views import View

from tara.api.controller import BaseMethodView, register_api
from tara.api.service.user import UserService

from tara.api.util.decorators import login_required
from tara.restful_login import current_user


class UserView(BaseMethodView):
    def __init__(self):
        self.service = UserService()


class UserChangePasswordView(View):
    def __init__(self):
        self.service = UserService()

    @login_required
    def dispatch_request(self):
        data = request.form
        self.service.change_password(current_user['_id'], data.get('old_password'), data.get('new_password'))
        return 'ok'


bp = Blueprint('user', __name__, url_prefix='/users')
bp.add_url_rule('/change_password', view_func=UserChangePasswordView.as_view('change_password'), methods=['POST'])

register_api(bp, UserView)
