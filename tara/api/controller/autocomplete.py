from flask import Blueprint

from tara.api.controller import BaseMethodView
from tara.api.service.autocomplete import AutocompleteService


class AutocompleteView(BaseMethodView):
    def __init__(self):
        self.service = AutocompleteService()

    def get(self, template_id, field):
        return self.service.find(template_id, field)


bp = Blueprint('autocomplete', __name__, url_prefix='/autocomplete')

view_func = AutocompleteView.as_view(bp.name)
view_func.provide_automatic_options = False

bp.add_url_rule('/<ObjectId:template_id>/<field>', view_func=view_func, methods=['GET'])
