class BaseConfig(object):
    DEBUG = True
    SSL = True


class ProductionConfig(BaseConfig):
    SEND_FILE_MAX_AGE_DEFAULT = 0
    SSL = True


class DevelopmentConfig(BaseConfig):
    MONGO_URI = 'mongodb://localhost:27017/tara'
    SEND_FILE_MAX_AGE_DEFAULT = 0


class TestingConfig(BaseConfig):
    TESTING = True
    MONGO_URI = 'mongodb://localhost:27017/tara_test'
