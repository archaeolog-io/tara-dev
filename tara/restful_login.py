from flask import _request_ctx_stack, current_app, request
from werkzeug.local import LocalProxy

#: A proxy for the current user. If no user is logged in, this will be an
#: anonymous user
current_user = LocalProxy(lambda: _get_user())


class LoginManager(object):
    def __init__(self, app=None, add_context_processor=True):

        self._user_loader = None

        self._unauthenticated_handle = None

        self._unauthorized_handle = None

        if app is not None:
            self.init_app(app, add_context_processor)

    def init_app(self, app, add_context_processor=True):
        app.login_manager = self
        app.before_request(self.load_user)

        if add_context_processor:
            app.context_processor(_user_context_processor)

    def load_user(self):
        _request_ctx_stack.top.user = self._user_loader()

    def unauthenticated(self):
        return self._unauthenticated_handle()

    def unauthorized(self):
        return self._unauthorized_handle()

    def user_loader(self, f):
        self._user_loader = f

    def unauthenticated_handle(self, f):
        self._unauthenticated_handle = f

    def unauthorized_handle(self, f):
        self._unauthorized_handle = f


def _get_user():
    try:
        return getattr(_request_ctx_stack.top, 'user')
    except AttributeError:
        return None


def _user_context_processor():
    return dict(current_user=_get_user())
