from flask.ext.pymongo import PyMongo
from flask.ext.bcrypt import Bcrypt
from restful_login import LoginManager

mongo = PyMongo()

bcrypt = Bcrypt()

login_manager = LoginManager()
